import {Component, OnInit} from '@angular/core';
import {Objective} from './models/objective';
import {Measure} from './models/measure';
import {Comment} from './models/comment';
import {ObjectiveService} from './services/objective.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    constructor(private objectiveService: ObjectiveService) {
    }

    objectives: Objective[];

    ngOnInit(): void {
        this.getObjectives();
    }

    private getObjectives() {
        this.objectiveService.getObjectives()
            .subscribe((objectives: Objective[]) => {
                this.objectives = objectives;
            });
    }
}
