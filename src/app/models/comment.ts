export class Comment {
    id: string;
    title: string;
    body: string;
    createdDate: string;
}
