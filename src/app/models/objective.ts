import {Measure} from './measure';

export class Objective {
    id: string;
    name: string;
    startDate: string;
    endDate: string;
    measures: Measure[];
}
