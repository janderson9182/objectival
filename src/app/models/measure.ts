import {Comment} from './comment';

export class Measure {
    id: string;
    objectiveId: string;
    description: string;
    amountCompleted: number;
    comments: Comment[];
}
