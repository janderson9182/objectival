import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Objective} from '../models/objective';

@Injectable({
    providedIn: 'root'
})
export class ObjectiveService {

    constructor(private httpClient: HttpClient) {
    }

    getObjectives(): Observable<Objective[]> {
        return this.httpClient.get<Objective[]>('http://localhost:3000/objectives');
    }
}
